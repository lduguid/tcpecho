# defines
CC=gcc
CFLAGS=-c -Wall
LDFLAGS=


all: TCPEchoClient TCPEchoServer


TCPEchoClient: TCPEchoClient.o DieWithError.o
		$(CC) TCPEchoClient.o DieWithError.o -o TCPEchoClient

TCPEchoServer: TCPEchoServer.o DieWithError.o HandleTCPClient.o
		$(CC) TCPEchoServer.o DieWithError.o HandleTCPClient.o -o TCPEchoServer 


TCPEchoClient.o: TCPEchoClient.c
		$(CC) $(CFLAGS) TCPEchoClient.c 	

DieWithError.o: DieWithError.c
		$(CC) $(CFLAGS) DieWithError.c

TCPEchoServer.o: TCPEchoServer.c
		$(CC) $(CFLAGS) TCPEchoServer.c

HandleTCPClient.o: HandleTCPClient.c
		$(CC) $(CFLAGS) HandleTCPClient.c

clean:
	rm -rf *.o TCPEchoClient TCPEchoServer
